<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Turing!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section>
        <title>Végtelen ciklus</title>
        <para>
            Írj olyan C végtelen ciklusokat, amelyek 0 illetve 100 százalékban dolgoztatnak egy magot és egy olyat, amely  
            100 százalékban minden magot!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/lvmi6tyz-nI">https://youtu.be/lvmi6tyz-nI</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Turing/infty-f.c">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/infty-f.c</filename>
            </link>, 
            <link xlink:href="Turing/infty-w.c">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/infty-w.c</filename>
            </link>.
        </para>
        <para>
            Számos módon hozhatunk és hozunk létre végtelen ciklusokat. 
            Vannak esetek, amikor ez a célunk, például egy szerverfolyamat fusson folyamatosan és van amikor egy
            bug, mert ott lesz végtelen ciklus, ahol nem akartunk. Saját péláinkban ilyen amikor a PageRank algoritmus
            rázza az 1 liter vizet az internetben, de az iteráció csak nem akar konvergálni...
        </para>                    
        <para>
            Egy mag 100 százalékban:               
        </para>
        <programlisting language="c"><![CDATA[int
main ()
{
  for (;;);

  return 0;
}
]]>
        </programlisting>        
        <para>        
        vagy az olvashatóbb, de a programozók és fordítók (szabványok) között kevésbé hordozható
        </para>
        <programlisting language="c"><![CDATA[int
#include <stdbool.h>
main ()
{
  while(true);

  return 0;
}
]]>
        </programlisting>        
        <para>
            Azért érdemes a <literal>for(;;)</literal> hagyományos formát használni, 
            mert ez minden C szabvánnyal lefordul, másrészt
            a többi programozó azonnal látja, hogy az a végtelen ciklus szándékunk szerint végtelen és nem szoftverhiba. 
            Mert ugye, ha a <literal>while</literal>-al trükközünk egy nem triviális 
            <literal>1</literal> vagy <literal>true</literal> feltétellel, akkor ott egy másik, a forrást
            olvasó programozó nem látja azonnal a szándékunkat.
        </para>            
        <para>
            Egyébként a fordító a <literal>for</literal>-os és 
            <literal>while</literal>-os ciklusból ugyanazt az assembly kódot fordítja:
        </para>            
        <screen><![CDATA[$ gcc -S -o infty-f.S infty-f.c 
$ gcc -S -o infty-w.S infty-w.c 
$ diff infty-w.S infty-f.S 
1c1
< 	.file	"infty-w.c"
---
> 	.file	"infty-f.c"
]]></screen>  
        <para>
            Egy mag 0 százalékban:               
        </para>        
        <programlisting language="c"><![CDATA[#include <unistd.h>
int
main ()
{
  for (;;)
    sleep(1);
    
  return 0;
}
]]>
        </programlisting>        
        <para>
            Minden mag 100 százalékban:               
        </para>

        <programlisting language="c"><![CDATA[#include <omp.h>
int
main ()
{
#pragma omp parallel
{
  for (;;);
}
  return 0;
}
]]>
        </programlisting>        
        <para>
            A <command>gcc infty-f.c -o infty-f -fopenmp</command> parancssorral készítve a futtathatót, majd futtatva,               
            közben egy másik terminálban a <command>top</command> parancsot kiadva tanulmányozzuk, mennyi CPU-t használunk:            
        </para>
        <screen><![CDATA[top - 20:09:06 up  3:35,  1 user,  load average: 5,68, 2,91, 1,38
Tasks: 329 total,   2 running, 256 sleeping,   0 stopped,   1 zombie
%Cpu0 :100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu1 : 99,7 us, 0,3 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu2 :100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu3 : 99,7 us, 0,3 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu4 :100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu5 :100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu6 :100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
%Cpu7 :100,0 us, 0,0 sy, 0,0 ni, 0,0 id, 0,0 wa, 0,0 hi, 0,0 si, 0,0 st
KiB Mem :16373532 total,11701240 free, 2254256 used, 2418036 buff/cache
KiB Swap:16724988 total,16724988 free,       0 used. 13751608 avail Mem 

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU %MEM     TIME+ COMMAND     
 5850 batfai    20   0   68360    932    836 R 798,3  0,0   8:14.23 infty-f     
]]></screen>  
                                
        <tip>
            <title>Werkfilm</title>
            <itemizedlist>
                <listitem>
                    <para>
                        <link xlink:href="https://youtu.be/lvmi6tyz-nI"> https://youtu.be/lvmi6tyz-nI</link>
                    </para>    
                </listitem>                
            </itemizedlist>                
        </tip>
    </section>        
        
 <section>
        <title>"Lefagyott, nem fagyott, akkor most mi van?"</title>
        <para>
            I.feladat: Mutasd meg, hogy nem lehet olyan programot írni, amely bármely más programról eldönti, hogy le fog-e fagyni vagy sem!
        </para>
        <para>
            Megoldás videó: Nem készítettem, mivel a program nem létrehozható, nincs miről megoldás videót készítenem.:)

        </para>
        <para>
            Nézzük meg, hogy elméletben hogyan lenne elkészíthető a program és miért nem kivitelezhető, lépésről lépésre.

        </para>
        <para>
            1. Kezdésként létrehozunk egy függvényt, amelyet később újra meghívhatunk a programban.
            A <function>Freeze</function> függvény boolean típusú, mely segítségével a 'Program A'-ról
            (egy tetszőleges program) el szeretnénk dönteni, hogy tartalmaz-e végtelen ciklust:
            ha tartalmaz, akkor a boolean egy igaz (true) értékkel tér vissza, különben hamis (false) értékkel.
	        Ezt a függvényt hívjuk meg a tényleges program (amely persze nem létezik) main részében.
            A képzeletünkben létező T100 program és az ahhoz szükséges <function>Freeze</function> függvény kódja:

        </para>
        <programlisting language="c"><![CDATA[Program T100
        {

	    boolean Freeze(Program A)
	        {
		 if(A-ban van végtelen ciklus)
			return true;
		 else
			return false; 
	        }

	        main(Input Q)
	        {
		    Freeze(Q)
	        }
            }]]></programlisting>    
        <para>
            A program futtatása, például akár az előző <filename>v.c</filename> ilyen pszeudókódjára:

            <screen><![CDATA[T100(t.c.pseudo)
            true]]></screen>            
            akár önmagára
            <screen><![CDATA[T100(T100)
            false]]></screen>  
            ezt a kimenetet adja.          
        </para>
        <para>
            A T1000-es program, amelyben a Freeze-re épülő Freeze2 már nem tartalmaz feltételezett,
            csak konkrét kódot (magyarázat a kód után):

        </para>
        <programlisting language="c"><![CDATA[Program T1000
            {

	        boolean Freeze(Program P)
	        {
		 if(P-ben van végtelen ciklus)
			return true;
		 else
			return false; 
	        }

	        boolean Freeze2(Program P)
	        {  
		 if(Freeze(P))
			return true;
		 else
			for(;;); 
        	}

	        main(Input Q)
	        {
	        Freeze2(Q)
	        }

            }]]></programlisting>            
            <programlisting><![CDATA[]]></programlisting>           

        <para>
            2. A T1000-es program legelején látható a Freeze függvény, amelyet már előbb levezettünk.
            Következő lépésként létrehoztunk egy újabb boolean típusú függvényt Freeze2 néven.
            Tehát a programnak két funkciója lesz. A második funkció/függvény ugyanúgy egy értéket (egy programot)
            kér és boolean értéket ad vissza (true/false), ezen belül az if elágazásban, ha az adott P program lefagy,
            tehát Freeze(P)==true, akkor ez a funkció true-t ad vissza értékként. Azonban, ha a P program nem fagy le, tehát 
            Freeze(P)== false, akkor ez a program is belekerül egy végtelen ciklusba ("for(;;)").. A program main részébe pedig meg tudjuk hívni
            a tetszőleges Q programunkat, amin végrehajthatnánk ezen procedúrát.

        </para> 
        <para>
            Mit fog kiírni erre a <computeroutput>T1000(T1000)</computeroutput> futtatásra?              

            <itemizedlist>
                <listitem>
                    <para>Ha T1000 lefagyó, akkor nem fog lefagyni, kiírja, hogy true</para>                        
                </listitem>
                <listitem>
                    <para>Ha T1000 nem fagyó, akkor pedig le fog fagyni...</para>                        
                </listitem>
            </itemizedlist>
            akkor most hogy fog működni? Sehogy, mert ilyen <function>Lefagy</function>
            függvényt, azaz a T100 program nem is létezik.           
            </para>
            Magyarázat, tapasztalatok, tanulságok:
            -Turing óta tudjuk, hogy nem készíthető olyan gép/program amely képes egy másikról eldönteni,
            hogy le fog-e fagyni, vagy sem. Mivel ha a tetszőleges program amit ellenőrizni szeretnénk,
            tartalmaz végtelen ciklust, akkor ebbe belefutva már lefagy a programunk, ha viszont nem tartalmaz,
            akkor a második funkcióban lévő if elágazás else ágába fut bele a program, ahol ismét egy végtelen ciklus alakul ki.
            Szinte paradoxonként hat ez a körkörös folyamat, amely mindig végtelen ciklusba, tehát lefagyásba fog belefutni.
            Erre a magyarázat a Megállási probléma.
            (Az ábra az előadásanyag 25.oldalán megtalálható)
        <para>

        </para>
</section>       
                
<section>
        <title>Változók értékének felcserélése</title>
        <para>
         Írj olyan C programot, amely felcseréli két változó értékét, bármiféle logikai utasítás vagy kifejezés
         használata nélkül!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://bhaxor.blog.hu/2018/08/28/10_begin_goto_20_avagy_elindulunk">
            https://bhaxor.blog.hu/2018/08/28/10_begin_goto_20_avagy_elindulunk
            </link>
            Saját Megoldás Videó: <link xlink:href="https://youtu.be/IWpaleI4o0Y"> https://youtu.be/IWpaleI4o0Y </link>
        </para>
        <para>
            A program kódja:
        </para>
        <programlisting language="c"><![CDATA[Program Valtozocsere
                #include <stdio.h>
                int main() {
                printf("Ket valtozo ertekenek felcserelese \n"); 
                printf("Adjon meg ket szamot \n");

                int a, b;
                scanf("%d", &a);
                scanf("%d", &b);
                printf("Az a valtozo kezdetben= %d \n",a);
                printf("A b valtozo kezdetben= %d \n",b);
                a= a+b;
                b= a-b;
                a= a-b;
                printf("Az a valtozo vegul= %d \n",a);
                printf("A b valtozo vegul= %d \n",b);
                return 0;
                }

        ]]></programlisting>    
        <para>
            Tanulságok, tapasztalatok, magyarázat...
            A program alapvetőleg többféleképpen is megoldható lenne, mint a linkelt videóban látszódik,
            megoldható akár segédváltozó alkalmazásával is. Megoldható lenne még kizáró vagy alkalmazásával,
            de egyszerű műveletek, mint összeadás és kivonás használatával is. Mi most az utóbbit alkalmaztuk,
            és ezt fogom lépésenknét elmagyarázni az olvasónak.

        </para>

        <para>
            Először include-olni kell a C nyelvhez alkalmazható header-t:
            <programlisting language="c"><![CDATA[Program Valtozocsere
            #include <stdio.h>
            ]]></programlisting>  
        </para>
        <para>
            A printf-et alkalmazzuk az általunk kívánt szöveg/utasítás megjelenítésére a user számára.
            <programlisting language="c"><![CDATA[Program
            printf("Ket valtozo ertekenek felcserelese \n"); 
            printf("Adjon meg ket szamot \n");
            ]]></programlisting>
        </para>
        <para>
            A program lényeges része:
            I.lépésként deklarálnunk kell két változót, melyeknek az értékeit később fel fogjuk cserélni.
            Ez most két integer típusú változó lesz: a és b, így az int-et használjuk.
            <programlisting language="c"><![CDATA[Program
            int a, b;
            ]]></programlisting>

            A változóknak kell, hogy értéke legyen, amit a user adhat meg. Egy vagy több érték bekéréséhez C-ben a scanf használható
            <programlisting language="c"><![CDATA[Program Valtozocsere
            scanf("%d", &a);
            scanf("%d", &b);
            ]]></programlisting>  
            A <programlisting language="c"><![CDATA[Program "%d" ]]></programlisting> utal a számtípusra. Az and jel egy-egy memóriaterületet foglal le az "a" és "b" változóknak.
        </para>
        <para>
            II. lépés:
            Ha logikusan belegondolunk, 3 egyszerű művelettel megoldható a csere. Ha összeadjuk a-t és b-t egymással, majd 
            b-nek értékül adva a-ból(ami ekkor már a+b-vel egyenlő) kivonjuk b-t, akkor b érétke felveszi a értékét,
            hisz az összegükből elvettük b értékét, a értéke marad, amelyet b megkap. Tehát kezdeti(a)= b-vel megoldva.
            III.lépésként: már csak a-nak kell kezdeti(b) értékét felvennie. Ekkor a-nak értékül adva ha a-ból
            (ami még mindig a+b-vel egyenlő) kivonjuk b-t (ami itt már kezdeti(a) értékével egyenlő),
            akkor az a változó megkapja kezdeti(b) értékét.
            <programlisting language="c"><![CDATA[Program Valtozocsere
            a= a+b;
            b= a-b;
            a= a-b;
            ]]></programlisting>
        </para>
        <para>
            IV.lépés: ki kell íratnunk a megkapott értékeket. Erre szintén a printf lesz segítségünkre.
            <programlisting language="c"><![CDATA[Program
            printf("Az a valtozo vegul= %d \n",a);
            printf("A b valtozo vegul= %d \n",b);
            ]]></programlisting>

            A kiírni kívánt szöveget printf-nél idézőjelbe tesszük (ami nem érték/változó).
            Ahogy látjuk a "%d" itt is az adott változó típusára utal, majd "/n"-el új sort tudunk nyitni (mint egy enter).
            Végül csak be kell írni a kívánt változót: a-t, vagy b-t és készen is vagyunk!
            (A return 0; a program visszatérési értéke).
        </para>

</section>                   

<section>
        <title>Labdapattogás</title>
        <para>
            Először if-ekkel, majd bármiféle logikai utasítás vagy kifejezés
            használata nélkül írj egy olyan programot, ami egy labdát pattogtat a karakteres konzolon! (Hogy mit értek
            pattogtatás alatt, alább láthatod a videókon.)
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://bhaxor.blog.hu/2018/08/28/labdapattogas">https://bhaxor.blog.hu/2018/08/28/labdapattogas</link>
            Saját videó: <link xlink:href="https://youtu.be/miWWbLFIBcE">https://youtu.be/miWWbLFIBcE </link>
        </para>
        <para>
            Megoldás forrása:
        </para>
        <para>
            1.Labdapattogás if-el:
        </para>
        <programlisting language="c"><![CDATA[
            #include <stdio.h>
            #include <curses.h>
            #include <unistd.h>

            int main(void)
            {
                WINDOW*ablak;
                ablak= initscr();

                int x=0;
                int y=0;
                int xnov=1;
                int ynov=1;
                int mx,my;

                for(;;){
                    getmaxyx(ablak, my, mx);
                    mvprintw(y,x, "O");
                    refresh ();
                    usleep(50000);
                    clear();
                    x= x+xnov;
                    y= y+ynov;

                    if(x>=mx-1){
                    xnov=xnov*-1;
                    }
                    if(x<=0){
                    xnov= xnov*-1;
                    }
                    if(y<=0) {
                    ynov= ynov*-1;
                    }
                    if(y>=my-1){
                    ynov=ynov*-1;
                    };
                }
                    return 0;
                }
        ]]></programlisting> 
    <para>
            3. Labdapattogás bármiféle logikai utasítás vagy kifejezés használata nélkül
    </para>
        <programlisting language="c"><![CDATA[Program Labdapattogas_if_nelkul
            #include <stdio.h>
            #include <stdlib.h>
            #include <unistd.h>
            #include <curses.h>

            int main(){
            WINDOW *ablak;
            ablak= initscr();
            int mx=282,my=64,x1=0,x2=0,y1=0,y2=0; //teljes kijelzős módban pattog
                for(;;){
                    refresh();
                    usleep(25000);
                    x1= (x1-1) % mx;
                    x2= (x2+1) % mx;
                    y1= (y1-1) % my;
                    y2= (y2+1) % my;
                    clear ();
                    mvprintw (abs((y1+(my-y2))/2),abs((x1+(mx-x2))/2),"O");
                }
            return 0;
            }

        ]]></programlisting> 

            <para>
                Tanulságok, tapasztalatok, magyarázat...
                1.Labdapattogtatás if-el
                I.lépésként include-olni kell a programhoz szükséges headereket, ezekből 3 db van.
                <programlisting language="c"><![CDATA[Program
                #include <stdio.h>
                #include <curses.h>
                #include <unistd.h>
                ]]></programlisting>
                A curses.h header működéséhez a szokásos terminálos futtatáshoz, szükséges egy -lncurses nevezetű kapcsoló
                (gcc program.c -o program -lncurses).
            </para>
            <para>
                II.lépésként: Már a main-en belül szerkeszthetjük a programot.
                <programlisting language="c"><![CDATA[Program
                WINDOW*ablak;
                ablak= initscr();
                ]]></programlisting>

                A WINDOW az az ablak, ami "fel fog ugrani", maga a terminál, amelyben a labda pattogni fog.
                A következő sorban lévő ablak= initscr(); ezt az ablakot inicializálja, ennek segítségével nem fog megjelenni
                semmilyen szöveg az ablakon, csak a labda és a tiszta tér a pattogtatáshoz.
            
                <programlisting language="c"><![CDATA[Program
                int x=0;
                int y=0;
                int xnov=1;
                int ynov=1;
                int mx, my;
                ]]></programlisting>


                Ezekkel a sorokkal deklaráljuk az x,y,xnov,ynov,mx és my változókat, valamint értékkel látjuk el őket.
                Az x és y változók a labda kezdeti helyzetét jelölik (bal felső sarok), tekinthetjük tengelyeknek őket.
                Az xnov és ynov pedig jelöli, hogy mennyivel fog haladni a labda x és y irányában egyszerre.
                Az mx és my a határai lesznek az ablaknak ahonnan a labda vissza fog pattanni.
                <programlisting language="c"><![CDATA[Program
                for(;;)
                ]]></programlisting>

                A for ezen felépítése végtelen ciklust idéz elő, amiben a program többi része lesz megtalálható,
                hogy a labda a végtelenségig, vagy a ctrl+c lenyomásáig pattoghasson.

                <programlisting language="c"><![CDATA[Program
                getmaxyx(ablak, my, mx);
                mvprintw(y,x, "O");
                refresh ();
                usleep(50000);
                clear();
                x= x+xnov;
                y= y+ynov;
                ]]></programlisting>

                Ezekkel a sorokkal beállítjuk a max értékét, majd elhelyezzük az O betűt (a labdát) x és y helyén (kezdetben 0,0),
                majd a refresh újratölti nekünk az ablakot, hogy minden lépés látszódjon. Az usleep(50000) pedig 50000 miliszekundumra
                "megakasztja" a programot/várakoztatja, hogy a labda mozgása szemmel is követhető legyen. A clear() rész elhagyható,
                azonban, ha beleírjuk úgy mindig letörli az előzetesen kirajzolódott "O" betűket/labdákat, így látszatra csak egy labda
                fog pattogni. Végül, de nem utolsó sorban az x=x+xnov és y=y+ynov sorok változtatják x és y értékét, ez növeli a
                koordinátákat/ meghatározza minden körben a labda helyzetét.

                <programlisting language="c"><![CDATA[Program
                if(x>=mx-1){
                xnov=xnov*-1;
                }
                if(x<=0){
                xnov= xnov*-1;
                }
                if(y<=0) {
                ynov= ynov*-1;
                }
                if(y>=my-1){
                ynov=ynov*-1;
                }
                ]]></programlisting>

                Utolsó lépés: a program szinte legfontosabb lépése, amelyben "megfordítjuk" a labda irányát, az if-ek. 
                Itt vizsgáljuk, hogy az x és y eléri-e a falat. Ha kisebbek, vagy egyenlőek, mint 0, akkor negatív irányában
                belelógnának, vagy kimennének a látható perifériából, így itt xnov-ot és ynov-ot meg kell szorozni -1-el, hogy
                az ellenkező irányba haladjon tovább (pozitívba). Ha x és y nagyobbak, vagy egyenlőek, mint a maximumuk -1, akkor
                a labda kicsit, vagy teljesen láthatatlanná válna, pozitív irányban túlhaladná a "falat", ezért itt is vesszük a 
                -1-szeresét az xnov és ynovnak, tehát ismét irányt változtat (visszafelé).
            </para>
            <para>
                2. Labdapattogás if nélkül:
                Lényegében ez a program is rendkívül hasonlít az először bemutatott if-eshez. 
                Azonban tartalmaz jelentős különbségeket is, ez bonyolultabb műveleteket igényel.

                <programlisting language="c"><![CDATA[Program
                int mx=282,my=64,x1=0,x2=0,y1=0,y2=0;
                ]]></programlisting>

                Ebben a sorban a terminál teljes képernyőjéhez szükséges adatokat adtam meg x (szélesség) és y (hosszúság)
                maximumának, innen fog a labda visszapattanni. Deklaráltuk tehát a maxokat és x1,x2,y1,y2-t,
                amelyek a "tengelyekhez" viszonyításért felelősek, ezekben tároljuk a megnyitott ablak két oldalaitól megtett
                távolságot.
                <programlisting language="c"><![CDATA[Program
                x1= (x1-1) % mx;
                x2= (x2+1) % mx;
                y1= (y1-1) % my;
                y2= (y2+1) % my;
                Ezeknél az értékadásoknál x1,x2 és y1, valamint y2 helyzetét figyelembe véve,
                azoktól 1-el kisebb értékeknek és az mx, my-nak a modját kapjuk.
                Ezeket az értékeket felhasználva a következő képlettel kirajzolhatjuk a labdát:
                ]]></programlisting>

                <programlisting language="c"><![CDATA[Program
                mvprintw (abs((y1+(my-y2))/2),abs((x1+(mx-x2))/2),"O");
                ]]></programlisting>
            </para>
    </section>
        
    <section>
        <title>Szóhossz és a Linus Torvalds féle BogoMIPS</title>
        <para>
            Írj egy programot, ami megnézi, hogy hány bites a szó a gépeden, azaz mekkora az <type>int</type> mérete.
            Használd ugyanazt a while ciklus fejet, amit Linus Torvalds a BogoMIPS rutinjában! 
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/9KnMqrkj_kU">https://youtu.be/9KnMqrkj_kU</link>, 
            <link xlink:href="https://youtu.be/KRZlt1ZJ3qk">https://youtu.be/KRZlt1ZJ3qk</link>, 
            Saját videó:
            <link xlink:href="https://youtu.be/hLKRrMk1LdM ">https://youtu.be/hLKRrMk1LdM </link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="Turing/bogomips.c">
                <filename>bhax/thematic_tutorials/bhax_textbook_IgyNeveldaProgramozod/Turing/bogomips.c</filename>
            </link>
            <programlisting language="c"><![CDATA[
                #include <stdlib.h>
                #include <stdio.h>
                int main(void)
                {
                int a=1;
                int szamlalo=0;
                while (a !=0){
                        a<<=1;
                        ++szamlalo;
                }
                printf("Szó mérete: %d\n",szamlalo);
                return 0;
                }
            }]]></programlisting> 

        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat... 
            Először deklarálunk egy int típusú a változót, ennek értéke mondjuk legyen 1, de igazából bármilyen
            integer lehetne, a hossz mindig 32 bitet kell, hogy eredményezzen. Ennek tudatában készítsünk egy számláló
            változót, aminek értéke legyen 0, mert innen kezdi el majd a ciklusban hozzáadni minden iterációnál a bit
            haladását. A while ciklus addig megy, amíg az intünk nem lesz 0, tehát nem tolódik ki teljesen, ezt az eltolást
            a whileon belüli következő sor teszi lehetővé. Majd végül a számlálót növeljük 1-el minden tolódás után.
            Végül kiíratjuk a számlálót, ami megadja nekünk az int méretét.
                    </para>
    </section>                     

    <section>
        <title>Helló, Google!</title>
        <para>
            Írj olyan C programot, amely egy 4 honlapból álló hálózatra kiszámolja a négy lap Page-Rank 
            értékét!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/QhMhXA92xPE ">https://youtu.be/QhMhXA92xPE </link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href=""></link>
             <programlisting language="c"><![CDATA[
                #include <stdio.h>
                #include <math.h>
                int main() {
                double mat[4][4] = {
                {0.0, 0.0, 1.0/3.0, 0.0},
                {1.0, 1.0/2.0, 1.0/3.0, 1.0},
                {0.0, 1.0/2.0, 0.0, 0.0},
                {0.0, 0.0, 1.0/3.0, 0.0}
                };
                double pr[4] = {0.0,0.0,0.0,0.0}; 
                double ideiglenespr[4] = {1.0,1.0,1.0,1.0};
                for(int l =0;l<220;l++)
                {
                for(int i=0; i<4; i++)
                {
                for(int j=0; j<4; j++)
                {
                pr[i] =mat[i][j]*ideiglenespr[j]+ pr[i] ;
                } }
                for(int i=0; i<4; i++) {
                ideiglenespr[i] = pr[i]; }

                }
                double osszeg;
                for(int i=0; i<4; i++) {
                osszeg=osszeg+pr[i];
                }
                for(int i=0; i<4; i++)
                {
                    printf("%f\n", pr[i]/osszeg);
                }
                }
             ]]></programlisting>

        </para>
        <para>
            Tanulságok, tapasztalatok, magyarázat...
            A programot a main-en belül egy 4x4-es mátrix elkészítésével kezdjük. Ez tartalmazza az adott page-ek linkjeit.
            Következő lépésben egy double típusú vektort hoztunk létre a pagerankeknek, majd egy szintén double
            típusú ideiglenespagerank vektort. Majd egy for ciklus következik, amely lefutása befolyásolja a tizedesjegyek
            pontosságát ami a rangokat szimbolizálja (az a legnagyobb rangú amelyiknek a legmagasabb lesz az értéke). A következő for cilusokban biztosítjuk,
            hogy az iteráció a mátrix és a vektor mind a 4 honlapjának linkjére végrehajtódjon az ott látható művelet, ahol
            a pagerank vektornak (pr) értékül adjuk a két mátrix szorzatát. Ez a szorzás adja meg a kapott vektorban az aktuális pagerank értékeket.
            A következő for segítségével az ideiglenes pagerankek értékét változtatjuk meg az előbb kiszámoltakra.
            Végül egy összeg változót deklarálunk amihez hozzáadjuk a pr értékeket.
            Ez után a pr és az összeg hányadosát íratjuk ki, mert ez adja meg az arányokat, amik a honlapok pagerank értékeinek
            felelnek meg.
        </para>
    </section>
                                                                                                                                                                                                                                                                                                                                                        
    <section> xml:id="bhax-textbook-feladatok-turing.MontyHall">
        <title>A Monty Hall probléma</title>
        <para>
            Írj R szimulációt a Monty Hall problémára!  
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://bhaxor.blog.hu/2019/01/03/erdos_pal_mit_keresett_a_nagykonyvben_a_monty_hall-paradoxon_kapcsan">https://bhaxor.blog.hu/2019/01/03/erdos_pal_mit_keresett_a_nagykonyvben_a_monty_hall-paradoxon_kapcsan</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/MontyHall_R">https://gitlab.com/nbatfai/bhax/tree/master/attention_raising/MontyHall_R</link>
        <programlisting language="c"><![CDATA[
            #include <stdio.h>
            kiserletek_szama=10000000
            kiserlet = sample(1:3, kiserletek_szama, replace=T)
            jatekos = sample(1:3, kiserletek_szama, replace=T)
            musorvezeto=vector(length = kiserletek_szama)

            for (i in 1:kiserletek_szama) {

                if(kiserlet[i]==jatekos[i]){
                
                    mibol=setdiff(c(1,2,3), kiserlet[i])
                
                }else{
                
                    mibol=setdiff(c(1,2,3), c(kiserlet[i], jatekos[i]))
                
                }

                musorvezeto[i] = mibol[sample(1:length(mibol),1)]

            }

            nemvaltoztatesnyer= which(kiserlet==jatekos)
            valtoztat=vector(length = kiserletek_szama)

            for (i in 1:kiserletek_szama) {

                holvalt = setdiff(c(1,2,3), c(musorvezeto[i], jatekos[i]))
                valtoztat[i] = holvalt[sample(1:length(holvalt),1)]
                
            }

            valtoztatesnyer = which(kiserlet==valtoztat)


            sprintf("Kiserletek szama: %i", kiserletek_szama)
            length(nemvaltoztatesnyer)
            length(valtoztatesnyer)
            length(nemvaltoztatesnyer)/length(valtoztatesnyer)
            length(nemvaltoztatesnyer)+length(valtoztatesnyer)

        ]]></programlisting>
        </para>

        <para>
            Tanulságok, tapasztalatok, magyarázat...
        </para>
    </section>

    <section xml:id="Brun">
        <title>100 éves a Brun tétel</title>
        <para>
            Írj R szimulációt a Brun tétel demonstrálására!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/xbYhp9G6VqQ">https://youtu.be/xbYhp9G6VqQ</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/Primek_R">https://gitlab.com/nbatfai/bhax/blob/master/attention_raising/Primek_R</link>
        </para>

        <para>
            A természetes számok építőelemei a prímszámok. Abban az értelemben, 
            hogy minden természetes szám előállítható prímszámok szorzataként.
            Például 12=2*2*3, vagy például 33=3*11.
        </para>
        <para>
            Prímszám az a természetes szám, amely csak önmagával és eggyel 
            osztható. Eukleidész görög matematikus már Krisztus előtt tudta, 
            hogy végtelen sok prímszám van, de ma sem tudja senki, hogy 
            végtelen sok ikerprím van-e. Két prím ikerprím, ha különbségük 2.
        </para>
        <para>
            Két egymást követő páratlan prím között a legkisebb távolság a 2, 
            a legnagyobb távolság viszont bármilyen nagy lehet! Ez utóbbit 
            könnyű bebizonyítani. Legyen n egy tetszőlegesen nagy szám. 
            Akkor szorozzuk össze n+1-ig a számokat, azaz számoljuk ki az 
            1*2*3*… *(n-1)*n*(n+1) szorzatot, aminek a neve (n+1) faktoriális, 
            jele (n+1)!.
        </para>
        <para>
            Majd vizsgáljuk meg az a sorozatot:
        </para>    
        <para>
            (n+1)!+2, (n+1)!+3,… , (n+1)!+n, (n+1)!+ (n+1) ez n db egymást követő azám, ezekre (a jól ismert
            bizonyítás szerint) rendre igaz, hogy            
        </para>    
        <itemizedlist>
            <listitem>
                <para>(n+1)!+2=1*2*3*… *(n-1)*n*(n+1)+2, azaz 2*valamennyi+2, 2 többszöröse, így ami osztható kettővel</para>
            </listitem>
            <listitem>
                <para>(n+1)!+3=1*2*3*… *(n-1)*n*(n+1)+3, azaz 3*valamennyi+3, ami osztható hárommal</para>
            </listitem>
            <listitem>
                <para>...</para>
            </listitem>
            <listitem>
                <para>(n+1)!+(n-1)=1*2*3*… *(n-1)*n*(n+1)+(n-1), azaz (n-1)*valamennyi+(n-1), ami osztható (n-1)-el</para>
            </listitem>
            <listitem>
                <para>(n+1)!+n=1*2*3*… *(n-1)*n*(n+1)+n, azaz n*valamennyi+n-, ami osztható n-el</para>
            </listitem>
            <listitem>
                <para>(n+1)!+(n+1)=1*2*3*… *(n-1)*n*(n+1)+(n-1), azaz (n+1)*valamennyi+(n+1), ami osztható (n+1)-el</para>
            </listitem>
        </itemizedlist>
        <para>
            tehát ebben a sorozatban egy prim nincs, akkor a (n+1)!+2-nél 
            kisebb első prim és a (n+1)!+ (n+1)-nél nagyobb első 
            prim között a távolság legalább n.            
        </para>    
        <para>
            Az ikerprímszám sejtés azzal foglalkozik, amikor a prímek közötti 
            távolság 2. Azt mondja, hogy az egymástól 2 távolságra lévő prímek
            végtelen sokan vannak.
        </para>    
        <para>
            A Brun tétel azt mondja, hogy az ikerprímszámok reciprokaiból képzett sor összege, azaz
            a (1/3+1/5)+ (1/5+1/7)+ (1/11+1/13)+... véges vagy végtelen sor konvergens, ami azt jelenti, hogy ezek
            a törtek összeadva egy határt adnak ki pontosan vagy azt át nem lépve növekednek, 
            ami határ számot B<subscript>2</subscript> Brun konstansnak neveznek. Tehát ez
            nem dönti el a több ezer éve nyitott kérdést, hogy az ikerprímszámok halmaza végtelen-e? 
            Hiszen ha véges sok van és ezek
            reciprokait összeadjuk, akkor ugyanúgy nem lépjük át a B<subscript>2</subscript> Brun konstans értékét, 
            mintha végtelen 
            sok lenne, de ezek már csak olyan csökkenő mértékben járulnának hozzá a végtelen sor összegéhez, 
            hogy így sem lépnék át a Brun konstans értékét.     
        </para>
        <para>
            Ebben a példában egy olyan programot készítettünk, amely közelíteni próbálja a Brun konstans értékét.
            A repó <link xlink:href="../../../bhax/attention_raising/Primek_R/stp.r">
                <filename>bhax/attention_raising/Primek_R/stp.r</filename>
            </link> mevű állománya kiszámolja az ikerprímeket, összegzi
            a reciprokaikat és vizualizálja a kapott részeredményt.
        </para>
        <programlisting language="R">
<![CDATA[#   Copyright (C) 2019  Dr. Norbert Bátfai, nbatfai@gmail.com
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>

library(matlab)

stp <- function(x){

    primes = primes(x)
    diff = primes[2:length(primes)]-primes[1:length(primes)-1]
    idx = which(diff==2)
    t1primes = primes[idx]
    t2primes = primes[idx]+2
    rt1plust2 = 1/t1primes+1/t2primes
    return(sum(rt1plust2))
}

x=seq(13, 1000000, by=10000)
y=sapply(x, FUN = stp)
plot(x,y,type="b")
]]>
        </programlisting>        
        <para>
            Soronként értelemezzük ezt a programot:
        </para>                
        <programlisting language="R">
<![CDATA[ primes = primes(13)]]>
        </programlisting>        
        <para>
            Kiszámolja a megadott számig a prímeket.             
        </para>
        <screen>
<![CDATA[> primes=primes(13)
> primes
[1]  2  3  5  7 11 13
]]>
        </screen>
                
        <programlisting language="R">
<![CDATA[ diff = primes[2:length(primes)]-primes[1:length(primes)-1]]]>
        </programlisting>        
        <screen>
<![CDATA[> diff = primes[2:length(primes)]-primes[1:length(primes)-1]
> diff
[1] 1 2 2 4 2
]]>
        </screen>        
        <para>
            Az egymást követő prímek különbségét képzi, tehát 3-2, 5-3, 7-5, 11-7, 13-11.
        </para>
        <programlisting language="R">
<![CDATA[idx = which(diff==2)]]>
        </programlisting>        
        <screen>
<![CDATA[> idx = which(diff==2)
> idx
[1] 2 3 5
]]>
        </screen>              
        <para>
            Megnézi a <varname>diff</varname>-ben, hogy melyiknél lett kettő az eredmény, mert azok az ikerprím párok, ahol ez igaz.
            Ez a <varname>diff</varname>-ben lévő 3-2, 5-3, 7-5, 11-7, 13-11 külünbségek közül ez a 2., 3. és 5. indexűre teljesül.
        </para>
        <programlisting language="R">
<![CDATA[t1primes = primes[idx]]]>
        </programlisting>  
        <para>
            Kivette a primes-ból a párok első tagját. 
        </para>
        <programlisting language="R">
<![CDATA[t2primes = primes[idx]+2]]>
        </programlisting>        
        <para>
            A párok második tagját az első tagok kettő hozzáadásával képezzük.
        </para>
        <programlisting language="R">
<![CDATA[rt1plust2 = 1/t1primes+1/t2primes]]>
        </programlisting>        
        <para>
            Az 1/t1primes a t1primes 3,5,11 értékéből az alábbi reciprokokat képzi:
        </para>
        <screen>
<![CDATA[> 1/t1primes
[1] 0.33333333 0.20000000 0.09090909
]]>
        </screen>                      
        <para>
            Az 1/t2primes a t2primes 5,7,13 értékéből az alábbi reciprokokat képzi:
        </para>
        <screen>
<![CDATA[> 1/t2primes
[1] 0.20000000 0.14285714 0.07692308
]]>
        </screen>                      
        <para>
            Az 1/t1primes + 1/t2primes pedig ezeket a törteket rendre összeadja.
        </para>
        <screen>
<![CDATA[> 1/t1primes+1/t2primes
[1] 0.5333333 0.3428571 0.1678322
]]>
        </screen>                      
        <para>
            Nincs más dolgunk, mint ezeket a törteket összeadni a 
            <function>sum</function> függvénnyel.
        </para>
        
        <programlisting language="R">
<![CDATA[sum(rt1plust2)]]>
        </programlisting>    
        <screen>
<![CDATA[>   sum(rt1plust2)
[1] 1.044023
]]>
        </screen>            
        <para>
            A következő ábra azt mutatja, hogy a szumma értéke, hogyan nő, egy határértékhez tart, a 
            B<subscript>2</subscript> Brun konstanshoz. Ezt ezzel a csipettel rajzoltuk ki, ahol először a fenti 
            számítást 13-ig végezzük, majd 10013, majd 20013-ig, egészen 990013-ig, azaz közel 1 millióig.
            Vegyük észre, hogy az ábra első köre, a 13 értékhez tartozó 1.044023.
        </para>
        <programlisting language="R">
<![CDATA[x=seq(13, 1000000, by=10000)
y=sapply(x, FUN = stp)
plot(x,y,type="b")]]>
        </programlisting>          
        <figure>
            <title>A B<subscript>2</subscript> konstans közelítése</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="img/BrunKorok.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>A B<subscript>2</subscript> konstans közelítése</phrase>
                </textobject>
            </mediaobject>
        </figure>                             
        <tip>
            <title>Werkfilm</title>
            <itemizedlist>
                <listitem>
                    <para>
                        <link xlink:href="https://youtu.be/VkMFrgBhN1g">https://youtu.be/VkMFrgBhN1g</link>
                    </para>    
                </listitem>                
                <listitem>
                    <para>
                        <link xlink:href="https://youtu.be/aF4YK6mBwf4">https://youtu.be/aF4YK6mBwf4</link>
                    </para>    
                </listitem>                
            </itemizedlist>                
        </tip>
    </section>

</chapter>                
